﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace RSHelpers
{
    public class PostgreDbBase<TConnection, TDataAdapter> : Database<TConnection, TDataAdapter> where TConnection : DbConnection, new() where TDataAdapter : DbDataAdapter, new()
    {
        public override string QuerySelectAllColumnsWithZeroRows(string TableName)
        {
            return $"SELECT * FROM " + NamePrefix + TableName + NamePostfix + " LIMIT 0";
        }

        public override string FilterUserTablesFromSchema()
        {
            return "table_schema='public'";
        }

        public override string QueryGetInsertedIdentity(string TableName, string IdentityName)
        {
            return "SELECT CURRVAL(pg_get_serial_sequence('" + TableName + "','" + IdentityName + "')) id;";
        }

        public override string QueryGetLimit(string SelectQuery, string OrderBy, int Offset = 0, int RowCount = -1)
        {
            return SelectQuery + $"ORDER BY {OrderBy} OFFSET {Offset} {(RowCount < 0 ? "" : $" FETCH NEXT {RowCount} ROWS ONLY")}";
        }

        public string Server
        {
            get; set;
        }

        public string Database
        {
            get; set;
        }

        public string User
        {
            get; set;
        }

        public string Password
        {
            get; set;
        }

        public override string BoolFalseValue
        {
            get
            {
                return "FALSE";
            }
        }

        public override string BoolTrueValue
        {
            get
            {
                return "TRUE";
            }
        }

        public PostgreDbBase() : base()
        {
        }

        public PostgreDbBase(string Server, string Database, string Login, string Password, int Timeout = 10)
        {
            DbConnectionStringBuilder cb = new DbConnectionStringBuilder
            {
                { "Host", Server },
                { "Database", Database },
                { "Username", Login },
                { "Password", Password },
                { "Timeout", Timeout }
            };
            cs = cb.ConnectionString;
        }

        public PostgreDbBase<TConnection, TDataAdapter> Clone()
        {
            var o = new PostgreDbBase<TConnection, TDataAdapter>
            {
                _ds = this._ds.Clone(),
                cs = this.cs
            };
            foreach (var c in this.OriginalColumns)
                o.OriginalColumns.Add(c.Key, c.Value);
            return o;
        }
    }
}
