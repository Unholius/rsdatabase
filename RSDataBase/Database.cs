﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RSHelpers
{
    public interface IDataBase
    {
        /// <summary>
        /// Fills the DataTable with data from server
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="WhereClause">SQL filter expression</param>
        /// <param name="ColumnNames">Array of column names to retrieve from server</param>
        /// <param name="OrderBy">Sort rows in DataTable</param>
        /// <param name="Limit">Specifies the number of rows to return</param>
        /// <param name="Offset">Specifies the number of rows to skip</param>
        /// <returns></returns>
        DataTable Load(string TableName, string WhereClause = null, string[] ColumnNames = null, string[] OrderBy = null, int Limit = -1, int Offset = 0);

        /// <summary>
        /// Saves changes to server
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="PrimaryKeyName"></param>
        /// <param name="ColumnNames">Which columns must be updated</param>
        /// <param name="WhereClause">SQL filter expression</param>
        /// <returns></returns>
        int Save(string TableName, string[] ColumnNames = null, string WhereClause = null);

        object Insert(string TableName, string PrimaryKeyName, string[] ColumnNames, object[] Values);


        /// <summary>
        /// Execute delete query on server
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="WhereClause"></param>
        /// <returns></returns>
        int Delete(string TableName, string WhereClause);

        /// <summary>
        /// Executes the query
        /// </summary>
        /// <param name="SQL"></param>
        /// <returns></returns>
        DataTable Execute(string SqlQuery, string TableName = null, int Timeout = 3600, CommandType QueryType = CommandType.Text);


        /// <summary>
        /// Dynamically get database schema from server
        /// </summary>
        /// <returns></returns>
        bool Init();
    }

    public abstract class Database<TConnection, TDataAdapter>: IDataBase where TConnection : DbConnection, new() where TDataAdapter : DbDataAdapter, new()
    {
        #region Fields

        /// <summary>
        /// Connection string
        /// </summary>
        public string cs;

        public abstract string BoolTrueValue { get; }
        public abstract string BoolFalseValue { get; }

        public Dictionary<string, string[]> OriginalColumns = new Dictionary<string, string[]>();

        protected DataSet _ds
        {
            get; set;
        }

        #endregion Fields

        #region Properties

        /// <summary>
        /// Last connect exception
        /// </summary>
        public string ConnectException { get; private set; }

        /// <summary>
        /// Prefix for table and column names
        /// </summary>
        public static char NamePrefix { get; set; } = '\"';

        /// <summary>
        /// Postfix for table and column names
        /// </summary>
        public static char NamePostfix { get; set; } = '\"';

        /// <summary>
        /// Prefix for non numeric values
        /// </summary>
        public static char StrPrefix { get; set; } = '\'';

        /// <summary>
        /// Postfix for non numeric values
        /// </summary>
        public static char StrPostfix { get; set; } = '\'';

        public static char NameDelimiter { get; set; } = '.';

        public DataTable[] Tables
        {
            get
            {
                var result = new List<DataTable>();
                foreach (DataTable dt in _ds.Tables)
                {
                    result.Add(dt);
                }
                return result.ToArray();
            }
        }

        #endregion Properties

        #region Structures

        public enum SQLActionType
        {
            SELECT = 0,
            INSERT,
            UPDATE,
            DELETE
        }
        public struct DataChangedArgs
        {
            public SQLActionType SQLAction; // insert, update, delete, select
            public long DataSize;
            public string Query;
            public object Data;
            public string TableName;
        }

        #endregion Structures

        #region Events

        public event Action<DataChangedArgs> DataChanged;

        #endregion Events

        #region Extension

        /// <summary>
        /// Quickly get DataTable by name
        /// </summary>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public DataTable this[string TableName]
        {
            get
            {
                //if (ds == null) return null;
                //if (ds.Tables.IndexOf(TableName) < 0) return null;
                return _ds.Tables[TableName];
            }

            set
            {
                if (_ds == null) return;
                if (_ds.Tables.IndexOf(TableName) < 0) return;
                var dt = _ds.Tables[TableName];
                dt.Columns.Clear();
                var columns = new DataColumn[dt.Columns.Count];
                value.Columns.CopyTo(columns, 0);
                dt.Columns.AddRange(columns);
                dt.TableName = value.TableName;
                dt.Merge(value);
            }
        }

        public object this[string TableName, string ColumnName, string WhereClause]
        {
            get
            {
                if (_ds == null) return null;
                if (_ds.Tables.IndexOf(TableName) < 0) return null;
                var rows = _ds.Tables[TableName].Select(WhereClause);
                return string.IsNullOrEmpty(ColumnName)
                    ? rows.Length == 1 ? rows[0] : (object)(rows.Length > 1 ? rows : null)
                    : rows.Length == 1 ? rows[0][ColumnName] : rows.Length > 1 ? rows.Values(ColumnName) : null;
            }

            set
            {
                if (_ds == null) throw new Exception($"DataSet is not initialized");
                if (_ds.Tables.IndexOf(TableName) < 0) throw new Exception($"Table {TableName} isn't exists");
                var rows = this[TableName].Select(WhereClause);
                if (rows == null || rows.Length == 0) throw new Exception($"Rows not found. Filter: {WhereClause}");
                foreach (DataRow r in rows)
                {
                    r[ColumnName] = value;
                }
            }
        }

        public object this[string TableName, string ColumnName, int RowIndex]
        {
            get
            {
                return _ds == null ? null : _ds.Tables.IndexOf(TableName) < 0 ? null : this[TableName].Rows[RowIndex][ColumnName];
            }

            set
            {
                if (_ds == null) return;
                if (_ds.Tables.IndexOf(TableName) < 0) return;
                this[TableName].Rows[RowIndex][ColumnName] = value;
            }
        }

        public DataRow[] this[string TableName, string WhereClause, string[] OrderByColumns = null]
        {
            get
            {
                return this[TableName].Select(WhereClause);
            }
        }

        #endregion Extension

        /// <summary>
        /// Ф-ция должна возвращать корректный sql-запрос результатом которого должна быть пустая выборка, включающая в себя все колонки из таблицы TableName
        /// </summary>
        /// <param name="TableName">Имя таблицы</param>
        /// <returns></returns>
        public abstract string QuerySelectAllColumnsWithZeroRows(string TableName);

        public abstract string QueryGetInsertedIdentity(string TableName, string IdentityName);

        public abstract string FilterUserTablesFromSchema();

        public abstract string QueryGetLimit(string SelectQuery, string OrderBy, int Offset = 0, int RowCount = 0);

        /// <summary>
        /// Установление соединения с базой данных и открывает его
        /// </summary>
        /// <param name="ConnectionString"></param>
        /// <returns></returns>
        public TConnection Connect()
        {
            var con = new TConnection
            {
                ConnectionString = cs
            };
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                ConnectException = ex.Message;
            }
            return con;
        }

        /// <summary>
        /// Установление соединения с базой данных и открывает его
        /// </summary>
        /// <param name="ConnectionString"></param>
        /// <returns></returns>
        public TConnection Connect(string ConnectionString, out bool Result)
        {
            ConnectException = null;
            Result = false;
            cs = ConnectionString;
            var con = new TConnection
            {
                ConnectionString = cs
            };
            try
            {
                con.Open();
                Result = true;
            }
            catch (Exception ex)
            {
                ConnectException = ex.Message;
            }
            return con;
        }

        /// <summary>
        /// Delete rows in table directly in db without DataTable
        /// If WhereClause is empty delete all
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="WhereClause">If Empty delete all</param>
        /// <returns></returns>
        public int Delete(string TableName, string WhereClause)
        {
            int rowCount = -1;
            bool result = false;
            using (TConnection con = Connect(cs, out result))
            {
                var cmd = con.CreateCommand();
                cmd.CommandText = $"DELETE FROM {NamePrefix}{TableName}{NamePostfix} {(string.IsNullOrWhiteSpace(WhereClause) ? "" : " WHERE (" + WhereClause + ")")}";
                rowCount = cmd.ExecuteNonQuery();
            }
            return rowCount;
        }

        /// <summary>
        /// Insert directly to database without DataTable
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="PrimaryKeyName"></param>
        /// <param name="ColumnNames"></param>
        /// <param name="Values"></param>
        /// <returns></returns>
        public object Insert(string TableName, string PrimaryKeyName, string[] ColumnNames, object[] Values)
        {
            object id = null;
            bool result = false;
            using (TConnection con = Connect(cs, out result))
            {
                var cmd = con.CreateCommand();
                for (int i = 0; i < ColumnNames.Length; i++)
                {
                    var p = cmd.CreateParameter();
                    p.ParameterName = "@p_" + ColumnNames[i];
                    p.Value = Values != null && i < Values.Length ? Values[i] : DBNull.Value;
                    cmd.Parameters.Add(p);
                }
                cmd.CommandText = $"INSERT INTO {NamePrefix}{TableName}{NamePostfix} ({string.Join(",", ColumnNames)}) VALUES ({string.Join(",", ColumnNames.Select(x => "@p_" + x))})";
                cmd.ExecuteNonQuery();
                if (!string.IsNullOrWhiteSpace(PrimaryKeyName))
                {
                    var cmdIdentity = con.CreateCommand();
                    cmdIdentity.CommandText = QueryGetInsertedIdentity(TableName, PrimaryKeyName);
                    id = cmdIdentity.ExecuteScalar();
                }
            }
            return id;
        }

        public DataTable Load(string TableName, string WhereClause = null, string[] ColumnNames = null, string[] OrderBy = null, int Limit = -1, int Offset = 0)
        {
            string[] tableNames = null;
            if (TableName.Contains(",")) tableNames = TableName.Split(',');
            if (TableName.Contains("*")) tableNames = _ds.Tables.OfType<DataTable>().Where(dt => Regex.IsMatch(dt.TableName, TableName)).Select(dt => dt.TableName).ToArray();
            if (tableNames == null) tableNames = new string[] { TableName };
            foreach (var tableName in tableNames)
            {
                    //foreach (DataRelation dr in this[tableName].ChildRelations)
                    //{
                    //    dr.ChildTable.Clear();
                    //}
                    this[tableName].Clear();
                bool connectResult = false;
                if (ColumnNames == null && OriginalColumns.Keys.Contains(TableName)) ColumnNames = OriginalColumns[TableName];
                var selectQuery = this[tableName].GetSelectQuery(ColumnNames, $"{NamePrefix}", $"{NamePostfix}");
                if (WhereClause != null) selectQuery += " WHERE " + WhereClause;
                if (OrderBy != null) selectQuery += " ORDER BY " + OrderBy;
                if (Limit > 0 && OrderBy != null)
                {
                    selectQuery = QueryGetLimit(selectQuery, string.Join(",", OrderBy), Offset, Limit);
                }
                if (Limit == 0)
                {
                    selectQuery = QuerySelectAllColumnsWithZeroRows(TableName);
                }
                using (var con = Connect(cs, out connectResult))
                {
                    var da = (TDataAdapter)Activator.CreateInstance(typeof(TDataAdapter), selectQuery, con);
                    da.AcceptChangesDuringFill = true;
                    da.Fill(this[tableName]);
                }
                this[tableName].AcceptChanges();
                //
                //if (LoadChildTables)
                //{
                //    foreach (DataRow r in this[TableName].Rows)
                //    {
                //        foreach (DataRelation dr in this[tableName].ChildRelations)
                //        {
                //            Refresh(dr.ChildTable.TableName, dr.ChildColumns[0].ColumnName + "=" + r[dr.ParentColumns[0].ColumnName]);
                //        }
                //    }
                //}
            }
            return _ds.Tables.IndexOf(TableName) >= 0 ? this[TableName] : null;
        }


        /// <summary>
        /// Updates server database from dataset
        /// </summary>
        /// <param name="TableName"></param>
        public int Save(string TableName, string[] ColumnNames = null, string WhereClause = null)
        {
            return Save(this[TableName], ColumnNames, WhereClause);
        }

        public int Save(DataTable dt, string PrimaryKeyName, string[] ColumnNames = null, string Filter = null)
        {
            if (dt == null) return -1;
            var rowsAffectedCount = 0;
            var result = false;
            var idName = PrimaryKeyName;
            using (TConnection con = Connect(cs, out result))
            {
                var tran = con.BeginTransaction();
                var cmdIdentity = con.CreateCommand();
                cmdIdentity.CommandText = QueryGetInsertedIdentity(dt.TableName, idName);
                if (ColumnNames == null && OriginalColumns.Keys.Contains(dt.TableName)) ColumnNames = OriginalColumns[dt.TableName];
                var selectQuery = dt.GetSelectQuery(ColumnNames, $"{NamePrefix}", $"{NamePostfix}");

                //Устанавливаем порядок обработки строк: INSERT, UPDATE, DELETE
                var rowStates = new DataViewRowState[] { DataViewRowState.Added, DataViewRowState.ModifiedCurrent, DataViewRowState.Deleted };
                //int totalRowsCount = 0;
                foreach (DataViewRowState rowState in rowStates)
                {
                    var rows = dt.Select(Filter, null, rowState);
                    var columns = ColumnNames ?? dt.GetColumnNames().Split(',');

                    foreach (DataRow r in rows)
                    {
                        if (r == null) continue;
                        var cmd = con.CreateCommand();
                        cmd.Transaction = tran;
                        var pId = cmd.CreateParameter();
                        pId.ParameterName = $"@p_{idName}";

                        pId.Value = rowState == DataViewRowState.Deleted ? r[idName, DataRowVersion.Original] : r[idName];
                        cmd.Parameters.Add(pId);

                        if (rowState == DataViewRowState.Added)
                        {
                            cmd.CommandText = dt.GetInsertQuery(ColumnNames, "", "", !dt.Columns[idName].AutoIncrement);
                        }

                        if (rowState == DataViewRowState.Deleted)
                        {
                            cmd.CommandText = dt.GetDeleteQuery($"{NamePrefix}", $"{NamePostfix}");
                        }

                        foreach (var colName in columns)
                        {
                            var p = cmd.CreateParameter();
                            if (dt.Columns[colName].DataType.Name.Contains("Byte[]"))
                            {
                                p.DbType = DbType.Binary;
                            }
                            p.ParameterName = "@p_" + colName;
                            p.Value = r.RowState == DataRowState.Deleted ? r[colName, DataRowVersion.Original] ?? DBNull.Value : r[colName] ?? DBNull.Value;
                            cmd.Parameters.Add(p);
                        }

                        if (rowState == DataViewRowState.ModifiedCurrent)
                        {
                            cmd.CommandText = dt.GetUpdateQuery(PrimaryKeyName, ColumnNames);
                        }

                        var count = cmd.ExecuteNonQuery();
                        rowsAffectedCount += count;

                        if (count > 0 && DataChanged != null)
                        {
                            var args = new DataChangedArgs
                            {
                                Data = rows,
                                Query = cmd.CommandText,
                                TableName = dt.TableName
                            };
                            if (rowState == DataViewRowState.Added)
                                args.SQLAction = SQLActionType.INSERT;
                            if (rowState == DataViewRowState.Deleted)
                                args.SQLAction = SQLActionType.DELETE;
                            if (rowState == DataViewRowState.ModifiedCurrent)
                                args.SQLAction = SQLActionType.UPDATE;
                            DataChanged(args);
                        }

                        if (!string.IsNullOrWhiteSpace(idName) && rowState == DataViewRowState.Added && dt.Columns[idName].AutoIncrement)
                        {
                            //dt.Columns[idName].AutoIncrement = false;
                            //dt.Columns[idName].Unique = false;
                            var id = cmdIdentity.ExecuteScalar();
                            r[idName] = id;
                            //dt.Columns[idName].AutoIncrement = true;
                            //dt.Columns[idName].Unique = true;
                        }

                        cmd.Dispose();
                        cmd = null;
                        r.AcceptChanges();
                        //totalRowsCount++;
                    }
                }

                if (string.IsNullOrEmpty(Filter))
                    dt.AcceptChanges();
                tran.Commit();
            }
            return rowsAffectedCount;
        }

        public int Save(DataTable dt, string[] ColumnNames = null, string Filter = null)
        {
            return Save(dt, dt.GetIdName(), ColumnNames, Filter);
        }

        public bool Init()
        {
            bool result = false;
            using (TConnection con = Connect(cs, out result))
            {
                if (result)
                {
                    _ds = new DataSet();

                    //Получаем информацию о таблицах с сервера:
                    DataTable dtDefaultValues = null;
                    try
                    {
                        dtDefaultValues = Execute("SELECT * FROM information_schema.columns WHERE column_default is not null");
                    }
                    catch
                    {

                    }
                    var tableNames = con.GetSchema("Tables").Select(FilterUserTablesFromSchema());
                    if (tableNames.Length == 0) throw new Exception("No tables found in database.");
                    foreach (var r in tableNames)
                    {
                        var tableName = $"{r["table_name"]}".TrimStart(NamePrefix).TrimEnd(NamePostfix);
                        var dt = new DataTable(tableName);
                        _ds.Tables.Add(dt);
                        var query = QuerySelectAllColumnsWithZeroRows(tableName);//$"SELECT * FROM " + LDI + tableName + RDI + " LIMIT 0";

                        //Создаем объект типа DbAdapter с передачей параметров конструктору:
                        var da = (TDataAdapter)Activator.CreateInstance(typeof(TDataAdapter), query, con);
                        //Получаем сведения о колонках таблицы:
                        da.FillSchema(dt, SchemaType.Source);
                        ///TODO: check for other sqls
                        foreach (DataColumn col in dt.Columns)
                        {
                            if (col.AutoIncrement)
                            {
                                col.AutoIncrementSeed = -1;
                                col.AutoIncrementStep = -1;
                                continue;
                            }
                            if (dtDefaultValues != null)
                            {
                                var defValues = dtDefaultValues.Select($"column_name={StrPrefix}{col.ColumnName}{StrPostfix} and table_name={StrPrefix}{dt.TableName}{StrPostfix}");
                                if (defValues != null && defValues.Length == 1)
                                {
                                    var defaultValue = $"{defValues[0]["column_default"]}";
                                    if (col.DataType != typeof(System.String))
                                    {
                                        defaultValue = defaultValue.Trim('(', ')');
                                    }
                                    if (col.DataType == typeof(System.Boolean))
                                    {
                                        if (defaultValue == "0") defaultValue = BoolFalseValue;
                                        if (defaultValue == "1") defaultValue = BoolTrueValue;
                                    }
                                    if (col.DataType == typeof(System.Double) || col.DataType == typeof(System.Decimal))
                                    {
                                        defaultValue = defaultValue.Replace(".", ",");
                                    }
                                    var d = DateTime.Now;
                                    if (col.DataType == typeof(DateTime) && !string.IsNullOrWhiteSpace(defaultValue) && !DateTime.TryParse(defaultValue, out d))
                                    {
                                        defaultValue = d.ToString();
                                    }
                                    try
                                    {
                                        col.DefaultValue = Convert.ChangeType(defaultValue, col.DataType);
                                    }
                                    catch
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Execute sql query on server and return result as DataTable
        /// </summary>
        /// <param name="SqlQuery"></param>
        /// <param name="Timeout"></param>
        /// <returns></returns>
        public DataTable Execute(string SqlQuery, string TableName = null, int Timeout = 3600, CommandType QueryType = CommandType.Text)
        {
            bool success = false;
            var pks = new List<DataColumn>();
            DataTable result = new DataTable();
            using (var con = Connect(cs, out success))
            {
                var cmd = con.CreateCommand();
                cmd.CommandText = SqlQuery;
                cmd.CommandType = QueryType;
                cmd.CommandTimeout = Timeout;
                IDataReader rdr = cmd.ExecuteReader();
                DataTable schemaTable = rdr.GetSchemaTable();

                if (schemaTable != null)
                {
                    foreach (DataRow dataRow in schemaTable.Rows)
                    {
                        DataColumn dataColumn = new DataColumn();
                        var columnIndex = 1;//for duplicate names
                        var columnName = $"{dataRow["ColumnName"]}";
                        if (result.Columns.IndexOf(columnName) >= 0)
                        {
                            while (result.Columns.IndexOf($"{columnName}_{columnIndex}") >= 0)
                            {
                                columnIndex++;
                            }
                            columnName = $"{columnName}_{columnIndex}";
                        }
                        if (columnName.StartsWith("_"))
                        {
                            dataColumn.ColumnName = columnName.TrimStart('_');
                            pks.Add(dataColumn);
                        }
                        else
                            dataColumn.ColumnName = columnName;

                        dataColumn.DataType = Type.GetType(dataRow["DataType"].ToString());
                        //dataColumn.ReadOnly = (bool)dataRow["IsReadOnly"];
                        dataColumn.AutoIncrement = (bool)dataRow["IsAutoIncrement"];
                        dataColumn.Unique = (bool)dataRow["IsUnique"];
                        result.Columns.Add(dataColumn);
                    }
                    try
                    {
                        result.BeginLoadData();
                        while (rdr.Read())
                        {
                            DataRow dataRow = result.NewRow();
                            for (int i = 0; i < result.Columns.Count; i++)
                            {
                                dataRow[i] = rdr[i];
                            }
                            result.Rows.Add(dataRow);
                        }
                        result.EndLoadData();
                        if (pks.Count > 0) result.PrimaryKey = pks.ToArray();
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
            result.AcceptChanges();
            result.TableName = TableName;
            return result;
        }
    }
}
