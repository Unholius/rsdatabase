﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace RSHelpers
{
    public static class DataExtensions
    {
        /// <summary>
        /// Возвращает массив значений колонок строки
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="ColumnName"></param>
        /// <returns></returns>
        public static object[] Values(this DataRow[] rows, string ColumnName)
        {
            var lst = new List<object>();
            foreach (DataRow r in rows)
                lst.Add(r[ColumnName]);
            return lst.ToArray();
        }

        /// <summary>
        /// Convert value to specific type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <param name="ColumnName"></param>
        /// <param name="Filter"></param>
        /// <returns></returns>
        public static T Get<T>(this DataTable dt, string ColumnName, string Filter = null)
        {
            if (dt == null) return default(T);
            var rows = dt.Select(Filter);
            if (string.IsNullOrEmpty(ColumnName))
            {
                if (rows.Length == 1)
                {
                    return (T)Convert.ChangeType(rows[0], typeof(T));
                }
            }
            else
            {
                if (dt.Columns.IndexOf(ColumnName) < 0) return default(T);
                if (rows.Length == 1)
                    return (T)Convert.ChangeType(rows[0][ColumnName], typeof(T));
            }
            return default(T);
        }

        public static string GetIdName(this DataTable Table, char Separator = ',', string LDI = "", string RDI = "")
        {
            if (Table == null) return null;
            var result = "";
            foreach (DataColumn col in Table.PrimaryKey)
            {
                result += LDI + col.ColumnName + RDI + Separator;
            }
            result = result.Trim(Separator);
            return result;
        }

        public static string GetColumnNames(this DataTable Table, bool AddPrimaryKey = false, char Separator = ',', string LDI = "", string RDI = "", string[] ColumnNames = null)
        {
            var result = "";
            if (ColumnNames != null)
            {
                foreach (var columnName in ColumnNames)
                {
                    if (Table.Columns.IndexOf(columnName) < 0) throw new Exception($"Column {columnName} doesn't exists in {Table.TableName}");
                }
            }

            foreach (DataColumn col in Table.Columns)
            {
                if (col.Unique && !AddPrimaryKey) continue;
                if (ColumnNames != null && Array.IndexOf(ColumnNames, col.ColumnName) < 0) continue;
                result += LDI + col.ColumnName + RDI + Separator;
            }
            return result.Trim(Separator);
        }

        public static string GetSelectQuery(this DataTable Table, string[] ColumnNames = null, string LDI = "", string RDI = "", bool IncludePrimaryKey = true)
        {
            var selectQuery = "";
            selectQuery += "SELECT " + Table.GetColumnNames(IncludePrimaryKey, ',', LDI, RDI, ColumnNames);
            selectQuery += " FROM " + LDI + Table.TableName + RDI;
            return selectQuery;
        }

        public static string GetInsertQuery(this DataTable Table, string[] ColumnNames = null, string LDI = "", string RDI = "", bool IncludePrimaryKey = false)
        {
            var columnNames = Table.GetColumnNames(IncludePrimaryKey, ',', LDI, RDI, ColumnNames);
            return "INSERT INTO " + LDI + Table.TableName + RDI + "(" + columnNames + ") VALUES (" + Table.GetColumnNames(IncludePrimaryKey, ',', "@p_", "", ColumnNames) + ")";
        }

        public static string GetDeleteQuery(this DataTable Table, string LDI = "", string RDI = "")
        {
            var idName = Table.GetIdName();
            if (!string.IsNullOrWhiteSpace(idName))
                return "DELETE FROM " + LDI + Table.TableName + RDI + " WHERE " + LDI + idName + RDI + "=@p_" + idName;
            else
            {
                var columnNames = Table.GetColumnNames(false, ',');
                var deleteQuery = "DELETE FROM " + LDI + Table.TableName + RDI + " WHERE ";
                foreach (var colName in columnNames.Split(','))
                {
                    deleteQuery += "(" + LDI + colName + RDI + "=@p_" + colName + ")AND";
                }
                deleteQuery = deleteQuery.Substring(0, deleteQuery.Length - 3);
                return deleteQuery;
            }
        }

        public static string GetUpdateQuery(this DataTable Table, string PrimaryKeyName, string[] ColumnNames = null, string LDI = "", string RDI = "")
        {
            var result = "";
            if (string.IsNullOrEmpty(PrimaryKeyName)) PrimaryKeyName = Table.GetIdName();
            var columns = ColumnNames ?? Table.GetColumnNames().Split(',');
            result += $"UPDATE {LDI}{Table.TableName}{RDI} SET ";
            foreach (var colName in columns)
            {
                result += LDI + colName + RDI + "=@p_" + colName + ",";
            }
            result = result.Trim(',');
            result += " WHERE " + LDI + PrimaryKeyName + RDI + "=@p_" + PrimaryKeyName;
            return result;
        }

    }
}
