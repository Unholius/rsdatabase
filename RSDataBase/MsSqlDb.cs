﻿using RSHelpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace RSHelpers
{
    public class MsSqlDb : Database<SqlConnection, SqlDataAdapter>
    {
        public override string BoolFalseValue
        {
            get
            {
                return "FALSE";
            }
        }

        public override string BoolTrueValue
        {
            get
            {
                return "TRUE";
            }
        }
        public override string QuerySelectAllColumnsWithZeroRows(string TableName)
        {
            return $"SELECT TOP 0 * FROM " + NamePrefix + TableName + NamePostfix;
        }

        public override string FilterUserTablesFromSchema()
        {
            return "table_schema='dbo'";
        }

        public override string QueryGetInsertedIdentity(string TableName, string IdentityName)
        {
            return $"SELECT @@IDENTITY";
        }

        public override string QueryGetLimit(string SelectQuery, string OrderBy, int Offset = 0, int RowCount = 0)
        {
            return $"{SelectQuery} OFFSET {Offset} ROWS {(RowCount > 0 ? $"FETCH NEXT {RowCount} ROWS ONLY" : "")}";
        }
        public MsSqlDb(string ConnectionString): base()
        {
            cs = ConnectionString;
            NamePrefix = '[';
            NamePostfix = ']';
            if (!Init()) throw new Exception(ConnectException);
        }
        public MsSqlDb(string Server, string Database, string User, string Password, int Timeout = 10): base()
        {
            var cb = new SqlConnectionStringBuilder
            {
                InitialCatalog = Database,
                DataSource = Server
            };
            if (User == null || Password == null)
            {
                cb.IntegratedSecurity = true;
            }
            else
            {
                cb.UserID = User;
                cb.Password = Password;
                cb.ConnectTimeout = Timeout;
            }
            cs = cb.ConnectionString;
            NamePrefix = '[';
            NamePostfix = ']';
            if (!Init()) throw new Exception(ConnectException);
        }
    }
}
