﻿using RSHelpers;
using System;

namespace TestDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Test database helper for CRUD operations.");
            var db = new MsSqlDb(@".\SQLEXPRESS", "Employees", "sa", "1234");
            Console.WriteLine($"Employees rows count: {db.Load("Employee", "FirstName LIKE 'Иван'", OrderBy: new[] { "DateOfBirth" })}");
            var employeeId = db.Insert("Employee", "Id", new[] { "FirstName", "LastName", "DateOfBirth" }, new object[] { "Петр", "Александрович", new DateTime(1957, 11, 13) });
            db.Insert("EmployeeWork", "Id", new[] { "EmployeeId", "OrganisationName", "BeginWork", "EndWork", "PostName" }, new object[] { employeeId, "Рога и Копыта Inc", new DateTime(2000, 2, 8), new DateTime(2006, 4, 22), "Директор" });
            foreach (var row in db["Employee", "IsDeleted='true'"])
                row.Delete();
            db.Save("Employee");
            Console.ReadKey();
        }
    }
}
